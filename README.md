# Enterprise 🚀

![home](sign_in-horz.jpg)

### Tools
- **Axios** were used for consumption by Api
- **Styled-component** to work with customizable components
- **Typescript** as a development language facilitating the definition of types
- **React-Navigation** for navigation between screens
- **Yup** for form validation
- **Unform** to capture and set input values
- **Immer** helps produce less verbose reducers
- **Redux** for Global state control
- **Redux-Saga** for handling asynchronous requests within Redux




**Installing dependencies**

```
$ cd enterprise 
$ yarn 
```

### Mobile
* You need to have Android Studio installed and configured and your computer to run APP React Native;
* With all dependencies installed and the environment properly configured, you can now run the app;
* With Android Studio running and Emulador open:
* Or emulate on your physical device.


Android

```
$ cd enterprise 
$ yarn android 
```


