import React, { useState, useEffect } from 'react';
import { Image, ScrollView, Dimensions } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import { useDispatch, useSelector } from 'react-redux';

import LinearGradient from 'react-native-linear-gradient';
import { resetState } from '../../store/modules/show/actions';

import {
  Container,
  Header,
  Touchable,
  Row,
  Title,
  Body,
  ImageBody,
  ContenFilterImage,
  Content,
  TitleBody,
  Description,
  SafeArea,
  ContentLoading,
  FloatView,
} from './styles';

import arrow from '../../assets/arrow.png';
import overflow from '../../assets/Overflow.png';
import image from '../../assets/image.png';

import Loading from '../../components/Loading';

const Show: React.FC = () => {
  const [headerShown, setHeaderShown] = useState(false);

  const { enterprise } = useSelector(state => state.show);

  console.log(enterprise, 'Show Item');

  const navigation = useNavigation();
  const dispatch = useDispatch();

  const windowHeight = Dimensions.get('window').height;

  const handleGoBack = () => {
    dispatch(resetState());
    navigation.goBack();
  };

  return (
    <>
      {!enterprise && (
        <>
          <ContentLoading>
            <Loading />
          </ContentLoading>
        </>
      )}

      {enterprise && (
        <>
          <SafeArea>
            <ScrollView
              onScroll={event => {
                const scrolling = event.nativeEvent.contentOffset.y;

                if (scrolling > 50) {
                  setHeaderShown(true);
                } else {
                  setHeaderShown(false);
                }
              }}
              // onScroll will be fired every 16ms
              scrollEventThrottle={16}
            >
              <Container>
                <Header>
                  <Row>
                    <Touchable onPress={() => handleGoBack()}>
                      <Image source={arrow} />
                    </Touchable>

                    <Touchable>
                      <Image source={overflow} />
                    </Touchable>
                  </Row>

                  <Title>{enterprise.enterprise_name}</Title>
                </Header>
              </Container>

              <Body>
                <ImageBody
                  source={{
                    uri: `https://empresas.ioasys.com.br/${enterprise.photo}`,
                  }}
                />
                <ContenFilterImage />
              </Body>

              <Content>
                <TitleBody>
                  {enterprise.city}, {enterprise.country}
                </TitleBody>
                <Description>{enterprise.description}</Description>
              </Content>

              <LinearGradient
                style={{
                  width: '100%',
                  height: 109,
                  position: 'absolute',
                  marginTop: windowHeight * 0.87,
                  transform: [{ translateY: headerShown ? 155 : -20 }],
                }}
                colors={[' rgba(249, 250, 255, 0.3)', '#fafbff']}
              />
            </ScrollView>
          </SafeArea>
        </>
      )}
    </>
  );
};

export default Show;
